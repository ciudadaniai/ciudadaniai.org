---
ref: luisfelipe
lang: es
sede: brasil
name: Luis Felipe Lopes
function: Coordinador de proyectos
bio: Cientísta Político con maestría en gobernanza urbana y desarrollo local de Sciences Po París. Activista de los derechos de la población LGBT +.
image: lipe.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
