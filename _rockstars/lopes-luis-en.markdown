---
ref: luisfelipe
lang: en
sede: chile
name: Luis Felipe Lopes
function: Project Coordinator
bio: Political scientist with a master's degree in urban governance and local development from Sciences Po Paris. LGBTI+ rights activist.
image: lipe.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
