---
ref: egle
lang: en
sede: chile
name: Eglé Flores
function: Regional Coordinator
bio: Feminist biologist with 13 years of experience in design, management and evaluation of collective impact processes. She writes, grows plants and questions her own colonialisms.
image: egle.jpg
email: eflores@ciudadanointeligente.org
network_twitter: https://twitter.com/solshinee
network_linkedin: https://cl.linkedin.com/in/eglé-flores-43846b15
network_github:
active: true
---
