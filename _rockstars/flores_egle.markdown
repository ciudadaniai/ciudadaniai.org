---
ref: egle
lang: es
sede: chile
name: Eglé Flores
function: Coordinadora Regional
bio: Bióloga feminista con 13 años de experiencia en diseño, gestión y evaluación de procesos de incidencia e impacto colectivo. Escribe, cultiva plantas y cuestiona sus colonialismos.
image: egle.jpg
email: eflores@ciudadanointeligente.org
network_twitter: https://twitter.com/solshinee
network_linkedin: https://cl.linkedin.com/in/eglé-flores-43846b15
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
